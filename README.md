## Calendar
Функции:

 - добавление/редактирование событий;
 - быстрое добавление события;
 - переход по месяцам;
 - сохранение календаря в localStorage.


### How to use
  
1. Скопируем репозиторий `git clone https://gitlab.com/repeckaya/calendar.git`
2. Открыть с помощью `index.html`

#### Contact me
[Telegram](https://t.me/repetskaya_oksana)