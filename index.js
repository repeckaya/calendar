const script = document.createElement('script');
script.src = './addEvent.js';
document.head.appendChild(script);

let months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
let daysOfWeek = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
let Calendar = function (divId) {
    let d = new Date();
    this.month = d.getMonth();
    this.year = d.getFullYear();
    this.today = d.getDate();
    this.divId = divId;
};

// Переход к следующему месяцу
Calendar.prototype.nextMonth = function () {
    if (this.month === 11) {
        this.month = 0;
        this.year = this.year + 1;
    } else {
        this.month = this.month + 1;
    }
    this.drawNecessaryMonth();
};

// Переход к предыдущему месяцу
Calendar.prototype.previousMonth = function () {
    if (this.month === 0) {
        this.month = 11;
        this.year = this.year - 1;
    } else {
        this.month = this.month - 1;
    }
    this.drawNecessaryMonth();
};
// Отрисовать текущий месяц
Calendar.prototype.drawCurrentMonth = function () {
    let d = new Date();
    this.month = d.getMonth()
    this.year = d.getFullYear()
    this.drawNecessaryMonth();
};

// Отрисовать необходимый месяц
Calendar.prototype.drawNecessaryMonth = function () {
    this.drawMonth(this.year, this.month);
};

// Отрисовать месяц
Calendar.prototype.drawMonth = function (y, m) {
    let firstDayOfMonth = new Date(y, m, 7).getDay(); // Первый день недели в выбранном месяце
    let lastDateOfMonth = new Date(y, m + 1, 0).getDate(); // Последний день выбранного месяца
    let lastDayOfLastMonth = m === 0 ? new Date(y - 1, 11, 0).getDate() : new Date(y, m, 0).getDate(); // Последний день предыдущего месяца

    let html = '<table>';
    // Запись выбранного месяца и года
    getId('current-month').innerHTML = months[m] + ' ' + y;

    // Записываем дни
    let i = 1;
    do {
        let dayofweek = new Date(y, m, i).getDay();
        if (dayofweek === 1) {
            html += '<tr>';
        }
        // Если первый день недели не понедельник, показать последние дни предыдущего месяца
        else if (i === 1) {
            html += '<tr>';
            let k = lastDayOfLastMonth - firstDayOfMonth + 1;
            for (let j = 0; j < firstDayOfMonth; j++) {
                if (this.month === 0) {
                    let month = 11;
                    let year = this.year - 1;
                    let uniqid = String(year) + String(month) + String(k);
                    html += `<td id="${uniqid}">` + k + '</td>';
                } else {
                    let month = this.month - 1;
                    let uniqid = String(this.year) + String(month) + String(k);
                    html += `<td id="${uniqid}">` + k + '</td>';
                }
                k++;
            }
        }
        // Записываем текущий день в цикл
        let d = new Date();
        let currentYear = d.getFullYear();
        let currentMonth = d.getMonth();
        let uniqid = String(this.year) + String(this.month) + String(i);
        if (currentYear === this.year && currentMonth === this.month && i === this.today) {
            html += `<td class="today" id="${uniqid}">` + i + '</td>';
        } else {
            html += `<td id="${uniqid}">` + i + '</td>';
        }

        if (dayofweek === 0) {
            html += '</tr>';
        }
        // Если последний день месяца не воскресенье, показать первые дни следующего месяца
        else if (i === lastDateOfMonth) {
            let k = 1;
            for (dayofweek; dayofweek < 7; dayofweek++) {
                if (this.month === 11) {
                    let month = 0;
                    let year = this.year + 1;
                    let uniqid = String(year) + String(month) + String(k);
                    html += `<td id="${uniqid}">` + k + '</td>';
                } else {
                    let month = this.month + 1;
                    let uniqid = String(this.year) + String(month) + String(k);
                    html += `<td id="${uniqid}">` + k + '</td>';
                }
                k++;
            }
        }
        i++;

    } while (i <= lastDateOfMonth);
    html += '</table>';

    // Записываем HTML в div
    document.getElementById(this.divId).innerHTML = html;

    // Добавляем дни недели
    let firstWeek = document.getElementById(this.divId).firstElementChild.rows[0]
    for (let num_days = 0; num_days <= 6; num_days++) {
        firstWeek.cells[num_days].innerHTML = daysOfWeek[num_days] + ', ' + firstWeek.cells[num_days].innerHTML;
    }
    // Добавляем каждой ячейке класс
    let elems = document.getElementsByTagName('td');
    for (let elem of elems) {
        elem.classList.add('cell');
        let key = elem.getAttribute('id');
        let data = JSON.parse(localStorage.getItem(`${key}`));
        if (data) {
            elem.style.backgroundColor = '#C2E4FE';
            elem.insertAdjacentHTML("beforeend", `<p class="event-short">${data.name}</p>`)
            elem.insertAdjacentHTML("beforeend", `<p class="description-short">${data.description}</p>`)
        }
    }
};

window.onload = function () {
    // Начать календарь
    let c = new Calendar("table");
    c.drawNecessaryMonth();
    getId('btnNext').onclick = function () {
        c.nextMonth();
    };
    getId('btnPrev').onclick = function () {
        c.previousMonth();
    };
    getId('update').onclick = function () {
        c.drawNecessaryMonth();
    };
    getId('extra-button').onclick = function () {
        c.drawCurrentMonth();
        document.querySelector('.today').style.color = 'blue';
        document.querySelector('.today').style.fontWeight = 'bold';
    };
}

function getId(id) {
    return document.getElementById(id);
}
// Быстрое добавление события
getId('quick-add').addEventListener('click', () => {
    let div = document.createElement('div');
    div.className = 'short-modal';
    document.body.insertBefore(div, getId('calendar'));
    div.insertAdjacentHTML("afterbegin", `<img id="cross" src="./assets/icons/cross.svg"  alt=""/>`)
    div.insertAdjacentHTML("beforeend", `<input id="shortdata" placeholder="25 ноября 2022, Шашлыки, Иван" />`)
    div.insertAdjacentHTML("beforeend", `<button id="button-create">Создать</button>`)

    getId('button-create').addEventListener('click', (e) => {
        let arr = getId('shortdata').value.split(',')
        let day = arr[0].split(' ')
        let date = day[0];
        let year = day[2];

        let month = day[1].charAt(0).toUpperCase() + day[1].slice(1);
        if (month.endsWith('я') || month.endsWith('а')) {
               if (month.startsWith('Март') || month.startsWith('Август')) {
                   month = month.slice(0, -1)
               } else if (month.startsWith('Ма')) { month = month.slice(0, -1) + 'й'} else {month = month.slice(0, -1) + 'ь'}
        }

        for (let i = 0; i < months.length; i++) {
            if (month === months[i]) {
                month = i;
            }
        }
        let id = String(year) + String(month) + String(date);
            localStorage.setItem(`${id}`, JSON.stringify({
            name: `${arr[1]}`,
            users: `${arr[2]}`,
        }));
    })
    cross(div)
})

// Закрыть по клику на крестик
function cross(div) {
    getId('cross').onclick = function () {div.remove()}
}
