// Добавление события через модальное окно
getId('table').addEventListener('click', e => {
    if (document.querySelector('.modal-content')) {
        document.querySelector('.modal-content').remove() //закрываем модальное окно, если оно уже не нужно
    }
    let id = e.target.getAttribute('id');
    let year = id.substr(0, 4);
    let m_d = id.substr(4);
    if (m_d.length === 2) {
        var month = months[m_d.substr(0, 1)]
    } else if (m_d.length === 4) {
        var month = months[m_d.substr(0, 2)]
    } else if (m_d.length === 3 && m_d.startsWith('10') || m_d.startsWith('11')) {
        var month = months[m_d.substr(0, 2)]
    } else if (m_d.length === 3) {
        var month = months[m_d.substr(0, 1)]
    }
    if (m_d.length === 2) {
        var day = m_d.substr(-1)
    } else if (m_d.length === 4) {
        var day = m_d.substr(-2, 2)
    } else if (m_d.length === 3 && m_d.startsWith('10') || m_d.startsWith('11')) {
        var day = m_d.substr(-1)
    } else if (m_d.length === 3) {
        var day = m_d.substr(-2, 2)
    }
    let str = day + ' ' + month + ', ' + year; // Показываем дату в выбранной ячейке

    let div = document.createElement('div');
    div.className = 'modal-content';
    document.body.insertBefore(div, getId('calendar'));
    let data = JSON.parse(localStorage.getItem(`${id}`));
    div.insertAdjacentHTML("afterbegin", `<div id="common" />`)
    getId('common').insertAdjacentHTML("afterbegin", `<p class="date">${str}</p>`)
    getId('common').insertAdjacentHTML("beforeend", `<img id="cross" src="./assets/icons/cross.svg"  alt=""/>`)
    if (data) {
        div.insertAdjacentHTML("beforeend", `<p class="event">${data.name}</p>`)
        div.insertAdjacentHTML("beforeend", `<p class="users">${data.users}</p>`)
        div.insertAdjacentHTML("beforeend", `<textarea class="description">${data.description}</textarea>`)
    } else {
        // создание модального окна Event add
        div.insertAdjacentHTML("beforeend", `<input class="event" type='text'  value="" placeholder="Событие"/>`)
        div.insertAdjacentHTML("beforeend", `<input class="users" type='text' placeholder="Имена участников"/>`)
        div.insertAdjacentHTML("beforeend", `<textarea class="description" placeholder="Описание"/>`)
    }
    div.insertAdjacentHTML("beforeend", `<button id="button-ready">Готово</button>`)
    div.insertAdjacentHTML("beforeend", `<button id="button-delete">Удалить</button>`)

    // Закрыть по клику на крестик
    cross(div)

    // Хранение в localstorage по кнопке "Готово"
    getId('button-ready').addEventListener('click', () => {
        localStorage.setItem(`${id}`, JSON.stringify({
            name: document.querySelector('.event').value,
            users: document.querySelector('.users').value,
            description: document.querySelector('.description').value
        }));
        div.remove();
    })

    // Удаление записи из localStorage по кнопке "Удалить"
    getId('button-delete').addEventListener('click', () => {
        let id = e.target.getAttribute('id');
        localStorage.removeItem(`${id}`);
        div.remove();
    })

})